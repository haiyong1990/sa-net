# SA-Net for 3D instance segmentation #

This repository contains the code for SA-Net: A Sample-and-Assign Network for End-to-end 3D Point Cloud InstanceSegmentation, CVPR2020. 


### Configuration ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
